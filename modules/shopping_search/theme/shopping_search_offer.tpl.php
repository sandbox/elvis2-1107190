<?php

// $Id: shopping_search_offer.tpl.php elvis2 Exp $

/**
 * @file shopping_search_offer.tpl.php
 *
 * This template handles shopping.com api product data on channel / category pages
 *
 * Variables available:
 * - $offer: An object containing data from shopping.com api item>offer element. Each $product contains:
 * - $offer->name: Offer name (product title)
 * - $offer->description: Offer description
 * - $offer->manufacturer: Brand or Manufacturer
 * - $offer->stockStatus: Availability. If available variable will be "In-stock"
 * - $offer->trusted: Is store Shopping.com Trusted
 * - $offer->logo: Store logo
 * - $offer->reviewCount: Number of store reviews
 * - $offer->rating: Store ratings
 * - $offer->store_name: Store name
 * - $offer->price: Product price
 * - $offer->image: Product image
 * - $offer->ratingImage: Store rating image
 * - $offer->url: URL to see store offer
 *
 * @see template_preprocess()
 * @see template_preprocess_shopping_search_offer()
 * 
 */
?>

<div class="offer">
  <div class="left">
    <?php if($offer->image): ?>
      <?php print l($offer->image, $offer->url->href, array(
												'html' => TRUE,
												'attributes' => array('target' => '_blank'),
												'query' => $offer->url->query,
												)
											);
			?>
    <?php endif; ?>
  
  </div>
  
  <div class="middle">
    <h4><?php print l($offer->name, $offer->url->href, array(
												'attributes' => array('target' => '_blank'),
												'query' => $offer->url->query,
												)
											);
				?>
	</h4>
    
    <?php if($offer->description): ?>
      <div class="description"><?php print $offer->description; ?></div>
    <?php endif; ?>
    
    <?php if($offer->manufacturer): ?>
      <div class="manufacturer"><span >Manufacture: </span><?php print $offer->manufacturer; ?></div>
    <?php endif; ?>
    
    <div class="store-name"><span >Store: </span><?php print $offer->store_name; ?></div>  
    <div class="store-logo"><?php print $offer->logo; ?></div>
    
    <?php if($offer->rating > 0): ?>
      <div class="rating"><span>Store Rating: </span><?php print $offer->ratingImage; ?></div>
    <?php endif; ?>

    <?php if($offer->trusted): ?>
      <div class="trusted">Trusted Store: Yes</div>
    <?php endif; ?>
  
  </div>
  
  <div class="right">  
      <div class="prices"><?php print l($offer->price, $offer->url->href, array(
																					'attributes' => array('target' => '_blank'),
																					'query' => $offer->url->query,
																				)
																			);
													?>
			</div>
      <div class="outbound"><?php print l('Go to store', $offer->url->href, array(
																					'attributes' => array('target' => '_blank'),
																					'query' => $offer->url->query,
																				)
																			);
														?>
			</div>
      <div class="stock">(<?php print $offer->stockStatus; ?>)</div>
  </div>
</div>