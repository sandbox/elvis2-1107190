<?php

// $Id: channel_product.tpl.php elvis2 Exp $

/**
 * @file channel_product.tpl.php
 *
 * This template handles shopping.com api product data on channel / category pages
 *
 * Variables available:
 * - $product: An object containing data from shopping.com api item>product element. Each $product contains:
 * - $product->name: Name of product
 * - $product->shortDescription: Short description, sometimes available
 * - $product->fullDescription: Full description, sometimes available
 * - $product->rating: Product ratings from shopping.com reviews
 * - $product->minPrice: If multiple stores, the lowest store price
 * - $product->maxPrice: If multiple stores, the highest store price
 * - $product->matchedOfferCount: Number of offers available
 * - $product->reviewCount: The number of reviews
 * - $product->url: Node URL
 * - $product->image: Product Image
 * - $product->ratingImage: Rating image from shopping.com api
 *
 * @see template_preprocess()
 * @see template_preprocess_channel_product()
 * 
 */
 
?>


<div id="product">
  <div class="left">
    <?php if($product->image): ?>
      <?php print l($product->image, $product->url, array('html' => TRUE, 'attributes' => array())); ?>
    <?php endif; ?>
  
  </div>
  
  <div class="middle">
    <h4><?php print l($product->name, $product->url, array('attributes' => array())); ?></h4>
    
    <?php if($product->shortDescription): ?>
      <div class="shortdescription"><?php print $product->shortDescription; ?></div>
    <?php elseif($product->fullDescription): ?>
      <div class="shortdescription"><?php print $product->fullDescription; ?></div>
    <?php endif; ?>
    
    <?php if($product->rating > 0): ?>
      <div class="rating"><span>Product Rating:</span><?php print $product->ratingImage; ?> (from <?php print $product->reviewCount; ?> reviews)</div>
    <?php endif; ?>
  
  </div>
  
  <div class="right">
    <?php if($product->matchedOfferCount == 1): // we only have one offer, so let's change the display a bit ?>
    
      <div class="prices"><?php print $product->minPrice;?></div>
      <div class="compare"><?php print l('See Details', $product->url, array('attributes' => array())); ?></div>
      
    <?php else: ?>
    
      <div class="prices"><?php print $product->minPrice;?> - <?php print $product->maxPrice; ?></div>
      <div class="compare"><?php print l('Compare Prices', $product->url, array('attributes' => array())); ?></div>
      <div class="stores">at <?php print $product->matchedOfferCount; ?> stores</div>
      
    <?php endif; ?>
    
  </div>
</div>