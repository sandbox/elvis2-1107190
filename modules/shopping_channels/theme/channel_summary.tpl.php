<?php
// $Id: channel_summary.tpl.php elvis2 Exp $

/**
 * @file channel_summary.tpl.php
 *
 * This template handles the category / channel
 * summary results from shopping.com api
 *
 * Variables available:
 * - $summary: An object containing data from shopping.com api category summary. Each $summary contains:
 * - $summary->matchedCategoryCount: Numer of matched categories. May not be useful
 * - $summary->returnedCategoryCount: Number of returned category count
 * - $summary->id:  Category Id
 * - $summary->name:  Category Name
 * - $summary->categoryURL:  Shopping.com Category URL
 * - $summary->contentType:  Shopping.com api content type
 * - $summary->matchedItemCount:  Total number of items available in this category
 * - $summary->pageNumber:  Shopping.com api page number
 * - $summary->returnedItemCount:  Returned items, controlled by channel settings
 *
 * @see node-shopping_channels.tpl.php
 * @see shopping_channels_view()
 * 
 */




?>