<div id="node-<?php print $node->type; ?>" class="node<?php if ($sticky) { print ' sticky'; } ?><?php if (!$status) { print ' node-unpublished'; } ?> clear-block">
  <div class="content">
    <?php print $node->content['item']['#value']; ?>
    <div class="stores"><?php print $node->content['stores']['#value']; ?></div>
  </div>
</div>