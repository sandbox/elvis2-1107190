<?php

// $Id: store.tpl.php elvis2 Exp $

/**
 * - $store_item: An object containing data from from shopping.com api product query. Each $store_item contains:
 * - $store_item->smartBuy: Shopping.com metric for trusted store and low price
 * - $store_item->used: Product used, if not then new
 * - $store_item->name: Product name
 * - $store_item->stockStatus: Availability, if available variable will output "In-stock"
 * - $store_item->storeNotes: Store notes about product, store offers, shipping info etc.
 * - $store_item->trusted: Shopping.com metric if store has consistantly held high reviews
 * - $store_item->url: URL to visit product offer and details
 * - $store_item->price: Product price
 * - $store_item->shipping: Shipping cost, if avialable
 * - $store_item->image: Product image
 *
 * @see template_preprocess()
 * @see template_preprocess_store_item()
 *
 */

$classes .= $store_item->truested ? $store_item->truested . ' ' : '';
$classes .= $store_item->smartBuy ? $store_item->smartBuy . ' ' : '';
$classes .= $store_item->shipping == 'Free Shipping' ? $store_item->smartBuy . ' ' : '';
$classes = trim($classes, " ");

?>

<div class="store-item">
  <p class="image"><?php print $store_item->image; ?></p>
  <p class="name"><?php print $store_item->name; ?></p>
  <p class="price"><?php print $store_item->price; ?></p>
  <p class="visit"><?php print l('Visit Store', $store_item->url->href, array(
																		'attributes' => array('target' => '_blank'),
																		'query' => $store_item->url->query,
																	)
																);
										?></p>
</div>