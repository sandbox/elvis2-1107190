<?php

// $Id: item.tpl.php elvis2 Exp $

/**
  $item->id
  $item->name
  $item->shortDescription
  $item->fullDescription
  $item->minPrice
  $item->maxPrice
  $item->reviewCount
  $item->rating
  $item->reviewURL
  $item->matchedOfferCount
  $item->pageNumber
  $item->returnedOfferCount
  $item->image
  $item->ratingImage
**/

$classes .= $item->manufacturer ? $item->manufacturer . ' ' : '';
$classes .= $item->categoryId ? $item->categoryId . ' ' : '';
$classes .= $item->manufacturer ? $item->manufacturer . ' ' : '';
$classes .= $item->smartBuy ? $item->smartBuy . ' ' : '';
$classes = trim($classes, " ");
?>

<div class="item <?php print $classes; ?>">
  <div class="left"><?php print $item->image; ?></div>
  
  <div class="right">
    <p><?php print $item->fullDescription ? $item->fullDescription : $item->shortDescription; ?></p>
    
    <?php if($item->rating): ?>
      <p><span>Product Rating: </span><?php print $item->ratingImage; ?></p>
    <?php endif; ?>
    
    <?php if($item->matchedOfferCount > 1): ?>
      <p><?php print $item->matchedOfferCount; ?> store offers from <?php print $item->minPrice; ?> to <?php print $item->maxPrice; ?></p>
    <?php endif; ?>
    
  </div>
</div>