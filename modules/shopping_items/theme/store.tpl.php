<?php

// $Id: store.tpl.php elvis2 Exp $

/**
  $store->smartBuy
  $store->used
  $store->name
  $store->stockStatus
  $store->storeNotes
  $store->trusted
  $store->reviewCount
  $store->rating
  $store->url
  $store->price
  $store->shipping
  $store->logo
  $store->ratingImage

**/

$classes .= $store->TRUEsted ? $store->TRUEsted . ' ' : '';
$classes .= $store->smartBuy ? $store->smartBuy . ' ' : '';
$classes .= $store->shipping == 'Free Shipping' ? $store->smartBuy . ' ' : '';
$classes = trim($classes, " ");

$name = $store->logo
				? l($store->logo, $store->url->href, array(
							'html' => TRUE,
							'attributes' => array('target' => '_blank'),
							'query' => $store->url->query,
						)
					)
				: l($store->name, $store->url->href, array(
							'attributes' => array('target' => '_blank'),
							'query' => $store->url->query,
						)
					)
				;
?>

<div class="<?php print $classes; ?>">
  <p><?php print $name; ?></p>
  <p>
    <?php print $store->trusted ? 'Trusted Store<br />' : '' ?>
    <?php print $store->ratingImage ? $store->ratingImage . '<br />' : ''; ?>
    <?php print $store->reviewCount ? $store->reviewCount . ' reviews' : ''; ?>
  </p>
  <p><?php print $store->storeNotes; ?></p>
  <p class="price"><?php print $store->price; ?></p>
</div>