<?php

// $Id: store.tpl.php elvis2 Exp $

/**
 *
 * - $store_hybrid->smartBuy
 * - $store_hybrid->used
 * - $store_hybrid->name
 * - $store_hybrid->stockStatus
 * - $store_hybrid->storeNotes
 * - $store_hybrid->trusted
 * - $store_hybrid->reviewCount
 * - $store_hybrid->rating
 * - $store_hybrid->url
 * - $store_hybrid->price
 * - $store_hybrid->shipping
 * - $store_hybrid->logo
 * - $store_hybrid->ratingImage
 * 

**/

$classes .= $store_hybrid->trusted ? $store_hybrid->trusted . ' ' : '';
$classes = trim($classes, " ");

$name = $store_hybrid->logo ? $store_hybrid->logo: $store_hybrid->name;
?>

<div class="store-custom">
  <p><?php print $name; ?></p>
  <p><?php print $store_hybrid->trusted ? 'Trusted Store<br />' : '' ?></p>
  <p><?php print $store_hybrid->ratingImage ? $store_hybrid->ratingImage . '<br />' : ''; ?></p>
  <p><?php print $store_hybrid->reviewCount ? $store_hybrid->reviewCount . ' reviews' : ''; ?></p>


</div>