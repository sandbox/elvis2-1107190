Requirements:
--------------------------
You need an shopping.com api partner account to get paid for your traffic. Find details here:
https://partners.shopping.com/Signup.html

The following modules are requred for sub-modules to work properly:
- url_alter: Is used to re-form urls with parameters.
- path: Is used for clean urls.


Warning:
--------------------------
Please read INSTALL.txt before installing all shopping modules. Some steps need to be exact for a successful install.

Thanks!
---------------------------
A special thanks to GYS Enterprises, LLC who sponsored the last leg of this module.


Description:
----------------------------
This module interacts with the shopping.com API (V3), to publish product data and offers. Learn more about the shopping.com API and becoming a publisher visit:
http://www.elvisblogs.org/shoppingcom-api

Currently the module uses Drupal as a framework to parse the shopping.com XML results and display them through the Drupal node and theme system. If there is enough interest I would like to integrate this module to interact with cck and views. Want to sponsor this functionality? Consider chipping in:
http://elvis.chipin.com/shoppingcom-for-drupal


Examples:
-----------------------------
Currently there are two sites up using this module.

Cameraprices.com: a simple directory of camera related goods

BabyClothing.com: a site focused on baby and toddler clothing, toys and accessories


Author:
-----------------------------
Elvis McNeely (aka elvis2) (http://elvisblogs.org/drupal)