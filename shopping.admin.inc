<?php
// $Id$


/**
 * @file shopping.admin.inc
 *  Controls module settings
 */

/**
 * hook_settings() implementation
 */
function shopping_settings() {

  $location = variable_get('shopping_locale', 'SandBox');
  $metadata = shopping_locales();
  $locale   = $metadata[$location];

  $options = array();
  foreach ($metadata as $opt => $data) {
    $options[$opt] = $data['name'];
  }

  $trackingId = variable_get('shopping_custom_associate_id', '');
  $trackingId = is_numeric($trackingId) ? $trackingId : $locale['trackingId'];

  $apiKey = variable_get('shopping_access_key', '');
  $apiKey = $apiKey ? $apiKey : $locale['apiKey'];

  $form['markup'] = array(
    '#type' => 'markup',
    '#value' => t('<p><b>Note:</b> Be sure to use your Shopping.com API credentials once you are ready to go live. Otherwise you will not get paid for your traffic.</p><p>Don\'t have Shopping.com API credentials yet? Signup as a partner here: '. l('partners.shopping.com', 'https://partners.shopping.com/Signup.html') .'</p>'),
  );

  $form['shopping_locale'] = array(
    '#type' => 'select',
    '#title' => t('Location'),
    '#default_value' => variable_get('shopping_locale', 'SandBox'),
    '#options' => $options,
    '#description' => t('Sandbox is a generic call to the USA version of the api. Some country locales have less product / category data. Save yourself time throughout development by choosing the locale you plan to use once you are live.'),
  );

  $form['shopping_custom_associate_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopping.com API Tracking ID'),
    '#description' => t('Enter your own associate ID to receive referral bonuses when shoppers purchase products via your site.'),
    '#default_value' => $trackingId,
  );

  $form['shopping_access_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Shopping.com API key'),
    '#description' => t('Applications must use a unique key to access Shopping.com services.'),
    '#default_value' => $apiKey,
  );

  $form['currency_symbol_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency Prefix'),
    '#description' => t('What symbol or characters to be used before currency value. ie: <b style="font-size: 120%;">$</b>99.99'),
    '#default_value' => variable_get('currency_symbol_prefix', '$'),
  );

  $form['currency_symbol_suffix'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency Suffix'),
    '#description' => t('What symbol or characters to be used after currency value. ie: 99.99 <b style="font-size: 120%;">USD</b>'),
    '#default_value' => variable_get('currency_symbol_suffix', ''),
  );

  return system_settings_form($form);
}

/**
 * hook_settings() implementation
 */
function shopping_caching_settings() {
  $form['shopping_explain'] = array(
    '#type' => 'markup',
    '#value' => t("<p>Cache data information in the Drupal database can bypass making a request to Shopping.com.</p>"),
  );

  $form['shopping_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Cache Shopping.com data locally. Highly recommened for busy websites.'),
    '#default_value' => variable_get('shopping_cache', FALSE),
  );


  //$period = array(0 => t('Never'));
  $period = drupal_map_assoc(array(900, 1800, 2700, 3600), 'format_interval');
  $form['shopping_cache_lifetime'] = array(
    '#type' => 'select',
    '#title' => t('Shopping.com refresh schedule'),
    '#description' => t("Cached information on Shopping.com items must be refreshed regularly to keep pricing and stock information up to date. Revenue accuracy is dependant on this feature. It is recommeneded to run cron every hour to recieve accuarate revenue credit. Cron must be enabled for this function to work properly."),
    '#default_value' => variable_get('shopping_cache_lifetime', 3600),
    '#options' => $period,
    '#prefix' => '<div id="shopping-cache-details">',
    '#suffix' => '</div>',
  );

  if (variable_get('shopping_cache_lifetime', FALSE) == 0) {
    $form['shopping_cache_details']['#prefix'] = '<div id="shopping-cache-details" class="js-hide">';
  }

  // Now add the Javascript that does the fancy hide/show effects.
  //drupal_add_js(drupal_get_path('module', 'shopping') .'/admin.js');

  return system_settings_form($form);
}

/**
 * hook_settings() implementation
 */
function shopping_debug_settings() {
  $form['shopping_blacklist'] = array(
    '#type' => 'markup',
    '#value' => t('<p>Use this feature to see the http request (URL to shopping.com) being made.</p>
                  <p>When filing an issue please turn on this feature and post URL parameters with the issue details.</p>'),
  );

  $form['shopping_debug']['shopping_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Turn on debug'),
    '#default_value' => variable_get('shopping_debug', FALSE),
  );

  return system_settings_form($form);
}

